
package myapp;

import java.io.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.net.URL;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.io.ByteArrayOutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;


public class ViewServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException,ServletException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();

        request.getRequestDispatcher("top.html").include(request, response);

        String clobData = null;
        String url;


        if (System.getProperty("com.google.appengine.runtime.version").startsWith("Google App Engine/")) {
            // Check the System properties to determine if we are running on appengine or not
            // Google App Engine sets a few system properties that will reliably be present on a remote
            // instance.
            url = System.getProperty("ae-cloudsql.cloudsql-database-url");
            try {
                // Load the class that provides the new "jdbc:google:mysql://" prefix.
                Class.forName("com.mysql.jdbc.GoogleDriver");
            } catch (ClassNotFoundException e) {
                throw new ServletException("Error loading Google JDBC Driver", e);
            }
        } else {
            // Set the url with the local MySQL database connection url when running locally
            url = System.getProperty("ae-cloudsql.local-database-url");
        }

        log("connecting to: " + url);
        final String selectSql = "SELECT unique_id, doc_type, XML_Col FROM swiftxml_tba ORDER BY timestamp DESC "
                + "LIMIT 20";
        try (Connection conn = DriverManager.getConnection(url);) {

            out.println("<H1>Displaying XMLDocument Store Records</h2>");
            out.println("<TABLE class='table table-bordered table-striped'>");
            out.println("<TR><TH>UniqueID<TH>Type<TH>XML</TR>");

            try (ResultSet rs = conn.prepareStatement(selectSql).executeQuery()) {
                out.print("Latest 20 Documents:\n");
                while (rs.next()) {
                    String unique_id = rs.getString("unique_id");
                    String doc_type = rs.getString("doc_type");
                    String xml_col = rs.getString("XML_Col");
                    Clob xml_col1 = rs.getClob ("XML_Col");

                    SQLXML xmlVal = rs.getSQLXML("XML_Col");
                    String val = xmlVal.getString();

                    Document doc = convertStringToDocument(xmlVal.getString());
                    String str = convertDocumentToString(doc);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(xml_col1.getAsciiStream()));
                    String read = null;
                    StringBuffer buffer = new StringBuffer();
                    while((read = reader.readLine()) != null )
                    {
                        buffer.append(read);
                    }
                    //String s = clobToString(xml_col1);
                    out.print("<TR><TD>" + unique_id + "<TD>" + doc_type + "<TD>" + str + "</TR>");
                }
                out.println("</TABLE>");

            }
            request.getRequestDispatcher("footer.html").include(request, response);
            out.close();
        } catch (SQLException e) {
            throw new ServletException("SQL error", e);
        }

    }


    private String clobToString(java.sql.Clob data)
    {
        final StringBuilder sb = new StringBuilder();

        try
        {
            final Reader reader = data.getCharacterStream();
            final BufferedReader br = new BufferedReader(reader);

            int b;
            while(-1 != (b = br.read()))
            {
                sb.append((char)b);
            }
            br.close();
        }
        catch (SQLException e)
        {

            return e.toString();
        }
        catch (IOException e)
        {

            return e.toString();
        }

        return sb.toString();
    }

    private static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try
        {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) );
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
