/*
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package myapp;

import java.io.*;
import java.util.*;
import javax.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.net.URL;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.io.ByteArrayOutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;


public class UploadServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException,ServletException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        request.getRequestDispatcher("top.html").include(request, response);

         //Integer id=Integer.parseInt(request.getParameter("uniqueID"));
        Integer id=67676;
        String uid=request.getParameter("uniqueID");
        String businessDate=request.getParameter("BusinessDate");
        String clobData = null;

        String url;
        String fileName = "BOOKS.XML";
        String sourceURL = "https://storage.googleapis.com/swiftxml_bucket/Books.xml";

        //out.println("<h4><font color='green'>id= " + uid + "</font></h4>");
        //out.println("<h4><font color='green'>BDAte= " + businessDate + "</font></h4>");

        if (System.getProperty("com.google.appengine.runtime.version").startsWith("Google App Engine/")) {
            // Check the System properties to determine if we are running on appengine or not
            // Google App Engine sets a few system properties that will reliably be present on a remote
            // instance.
            url = System.getProperty("ae-cloudsql.cloudsql-database-url");
            try {
                // Load the class that provides the new "jdbc:google:mysql://" prefix.
                Class.forName("com.mysql.jdbc.GoogleDriver");
            } catch (ClassNotFoundException e) {
                throw new ServletException("Error loading Google JDBC Driver", e);
            }
        } else {
            // Set the url with the local MySQL database connection url when running locally
            url = System.getProperty("ae-cloudsql.local-database-url");
        }

        log("connecting to: " + url);

        try (Connection conn = DriverManager.getConnection(url);) {

            try {
                clobData = getClobDataAsString(sourceURL);
                insertClob(conn, id, fileName, clobData);
            }
            catch (Exception e) {
                e.printStackTrace();
                out.println("<h4><font color='red'>Unable to insert " + e.getMessage() + "</font></h4>");
            }
            out.println("<h4><font color='green'>Successfully inserted Your Record with id= " + id + "</font></h4>");

        } catch (SQLException e) {
            throw new ServletException("SQL error", e);
        }
        request.getRequestDispatcher("footer.html").include(request, response);
        out.close();

    }

    public void insertClob(Connection con, Integer id, String fileName, String fileData)throws Exception{
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement("insert into swiftxml_tba(unique_id, doc_type, XML_Col) values (?, ?, ?)");
            ps.setInt(1, id);
            ps.setString(2, fileName);
            ps.setString(3, fileData);
            ps.executeUpdate();
        }catch(Exception e){
            System.out.println(e);
        }finally {
            ps.close();
        }
    }
    public static String getClobDataAsString(String urlData) throws Exception {
        InputStream is = null;
        try {
            URL url = new URL(urlData);
            System.out.println("url"+url);
            URLConnection urlConn = url.openConnection();
            urlConn.connect();
            is = urlConn.getInputStream();
            int BUFFER_SIZE = 1024;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            int length;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((length = is.read(buffer)) != -1) {
                output.write(buffer, 0, length);
            }
            return new String(output.toByteArray());
        } finally {
            is.close();
        }
    }

    private String clobToString(java.sql.Clob data)
    {
        final StringBuilder sb = new StringBuilder();

        try
        {
            final Reader reader = data.getCharacterStream();
            final BufferedReader br = new BufferedReader(reader);

            int b;
            while(-1 != (b = br.read()))
            {
                sb.append((char)b);
            }
            br.close();
        }
        catch (SQLException e)
        {

            return e.toString();
        }
        catch (IOException e)
        {

            return e.toString();
        }

        return sb.toString();
    }

    private static String convertDocumentToString(Document doc) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            // below code to remove XML declaration
            // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            String output = writer.getBuffer().toString();
            return output;
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try
        {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) );
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
